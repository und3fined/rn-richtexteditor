import { StatusBar } from "expo-status-bar";
import Constants from "expo-constants";
import React from "react";
import {
  SafeAreaView,
  SafeAreaProvider,
  SafeAreaInsetsContext,
  useSafeAreaInsets,
  initialWindowMetrics,
} from "react-native-safe-area-context";
import { StyleSheet, Text, View } from "react-native";

import Editor from "./RichTextEditor";

export default function App() {
  return (
    <View style={styles.container}>
      <SafeAreaView>
        <Editor />
        <StatusBar style="dark-content" translucent />
      </SafeAreaView>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});
