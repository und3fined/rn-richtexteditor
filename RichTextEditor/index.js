/**
 * File: index.js
 * Project: wpApps
 * File Created: 25 Nov 2020 09:48:08
 * Author: und3fined (me@und3fined.com)
 * -----
 * Last Modified: 25 Nov 2020 09:48:08
 * Modified By: und3fined (me@und3fined.com)
 * -----
 * Copyright (c) 2020 und3fined.com
 */

import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  View,
  KeyboardAvoidingView,
  useWindowDimensions,
  TouchableOpacity,
  Text,
} from "react-native";

import Editor from "./editor";
import Toolbar from "./toolbar";
import toolbarConfig from "./config";

import { createText } from "./helpers/block";

export default () => {
  const window = useWindowDimensions();
  const styled = styles();
  const iconColor = "#353232";

  const [toolbarActives, setToolbarActives] = useState([]);
  const [content, setContent] = useState([createText()]);

  const handleSelect = (toolActives) => {
    setToolbarActives(toolActives);
  };

  // console.info("-- content", content);

  return (
    <KeyboardAvoidingView
      style={{ flex: 1 }}
      behavior={Platform.OS == "ios" ? "padding" : "height"}
    >
      <Editor
        key="editor"
        hyperStyles={toolbarActives}
        onHyperStyles={setToolbarActives}
        content={content}
        onChangeContent={setContent}
        placeholder="placeholder"
      />
      <TouchableOpacity onPress={() => setContent([createText()])}>
        <Text style={{ height: 36, textAlign: "center", lineHeight: 36 }}>
          RESET
        </Text>
      </TouchableOpacity>
      <Toolbar
        key="toolbar"
        config={toolbarConfig(styled, iconColor)}
        seperatorStyle={{ color: "#999999" }}
        defaultColor="#111111"
        activeColor="#333333"
        activeBackground="#555555"
        actives={toolbarActives}
        onSelect={handleSelect}
        placeholder={"Nội dung..."}
      />
    </KeyboardAvoidingView>
  );
};

const styles = (theme) => {
  return StyleSheet.create({
    toolbarIcon: {
      width: 24,
      height: 24,
    },
  });
};
