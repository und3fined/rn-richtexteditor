/**
 * File: hook.js
 * Project: RichTextEditor
 * File Created: 25 Nov 2020 13:23:03
 * Author: und3fined (me@und3fined.com)
 * -----
 * Last Modified: 25 Nov 2020 13:23:03
 * Modified By: und3fined (me@und3fined.com)
 * -----
 * Copyright (c) 2020 und3fined.com
 */
import { useMemo, useEffect } from "react";

export const useWillMount = (func = () => {}) => {
  useMemo(func, []);
};

export const useDidMount = (func = () => {}) => {
  useEffect(func, []);
};
