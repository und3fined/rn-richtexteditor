/**
 * File: styles.js
 * Project: RichTextEditor
 * File Created: 25 Nov 2020 15:31:14
 * Author: und3fined (me@und3fined.com)
 * -----
 * Last Modified: 25 Nov 2020 15:31:14
 * Modified By: und3fined (me@und3fined.com)
 * -----
 * Copyright (c) 2020 und3fined.com
 */
import { StyleSheet } from "react-native";

export default StyleSheet.create({
  bold: { fontWeight: "700" },
  italic: { fontStyle: "italic" },
  underline: { textDecorationLine: "underline" },
  strike: { textDecorationLine: "line-through" },
  "underline strike": { textDecorationLine: "underline line-through" },
  "align-left": { textAlign: "left" },
  "align-right": { textAlign: "right" },
  "align-center": { textAlign: "center" },
  heading1: { fontSize: 15 },
  heading2: { fontSize: 15 },
  heading3: { fontSize: 15 },
  heading4: { fontSize: 15 },
  heading5: { fontSize: 15 },
  heading6: { fontSize: 15 },
  p: { fontSize: 15 },
  ul: {},
  ol: {},
});
