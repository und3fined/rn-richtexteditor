/**
 * File: fountain.js
 * Project: RichTextEditor
 * File Created: 25 Nov 2020 23:53:44
 * Author: und3fined (me@und3fined.com)
 * -----
 * Last Modified: 25 Nov 2020 23:53:44
 * Modified By: und3fined (me@und3fined.com)
 * -----
 * Copyright (c) 2020 und3fined.com
 */
import { nanoid } from "nanoid/non-secure";
import _ from "lodash";
import update from "immutability-helper";
import styles from "./styles";

export function updateBlock(content, index, items) {
  return update(content, { [index]: { content: { $set: items } } });
}

export function updateContentItem(
  items,
  index,
  { text, hyperStyles = [], tag = "", attrs = [] }
) {
  return update(items, {
    [index]: {
      text: { $set: text },
      hyperStyles: { $set: hyperStyles },
      tag: { $set: tag },
      attrs: { $set: attrs },
      len: { $set: text.length },
    },
  });
}

export function appendContent(items, item) {
  return update(items, { $push: [item] });
}

export function createContentItem(
  text = "",
  tag = "body",
  hyperStyles = [],
  styles = []
) {
  return {
    id: nanoid(),
    text,
    len: text.length,
    hyperStyles,
    attrs: [],
    styles,
    tag,
  };
}

export function createText(initialText = "") {
  return {
    id: nanoid(),
    component: "text",
    hyperStyles: [],
    attrs: [],
    styles: [],
    content: [createContentItem(initialText)],
  };
}

export function createImage(uri = "") {
  return {
    id: nanoid(),
    component: "image",
    hyperStyles: [],
    attrs: [],
    styles: [],
    content: [
      {
        id: nanoid(),
        src: uri,
        hyperStyles: [],
        attrs: [],
        styles: [],
        tag: "image",
      },
    ],
  };
}

export function findItem(content, cursorPosition) {
  let indx = 0;
  let findIndex = -1;
  let checknext = true;
  let itemIndex = 0;

  for (let index = 0; index < content.length; index++) {
    const element = content[index];

    const ending = indx + element.len;

    if (checknext === false) {
      if (element.len === 0) {
        findIndex = index;
        itemIndex = 0;
        break;
      } else {
        break;
      }
    }

    if (cursorPosition <= ending && cursorPosition >= indx) {
      findIndex = index;
      itemIndex = cursorPosition - indx;
      checknext = false;
    }

    indx += element.len;
  }

  if (findIndex == -1) {
    findIndex = content.length - 1;
  }

  return { findIndex, itemIndex };
}

export function findIndex(items, cursor) {
  let prevLen = 0;

  return _.findIndex(items, (item) => {
    console.info("prevLen---------- ", prevLen, cursor, item.len);
    if (prevLen === cursor) return true;
    if (prevLen < cursor && cursor <= prevLen + item.len) return true;

    prevLen += item.len;
    return false;
  });
}

export function getFulltext(items) {
  return items.reduce((txt, { text }) => (txt += text), "");
}

export function getFulltextLength(items) {
  return items.reduce((start, { len }) => (start += len), 0);
}

export function getPrevText(items, index) {
  return items.reduce((txt, { text }, idx) => {
    if (idx < index) return (txt += text);
    else return (txt += "");
  }, "");
}
