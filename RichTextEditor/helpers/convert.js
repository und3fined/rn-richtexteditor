/**
 * File: convert.js
 * Project: RichTextEditor
 * File Created: 25 Nov 2020 23:42:32
 * Author: und3fined (me@und3fined.com)
 * -----
 * Last Modified: 25 Nov 2020 23:42:33
 * Modified By: und3fined (me@und3fined.com)
 * -----
 * Copyright (c) 2020 und3fined.com
 */
import { DOMParser, XMLSerializer } from "xmldom";

export function convertToHtmlString(contents, styleList = null) {
  const availableStyles = styleList == null ? defaultStyles : styleList;

  // let keys = Object.keys(availableStyles);
  const myDoc = new DOMParser().parseFromString("<div></div>", "text/xml");

  for (let i = 0; i < contents.length; i++) {
    const input = contents[i];

    if (input.component === "text") {
      var element = null;
      let parent = null;
      let olStarted = false;
      let ulStarted = false;
      for (let j = 0; j < input.content.length; j++) {
        const item = input.content[j];
        const isBold = item.stype.indexOf("bold") > -1;
        const isItalic = item.stype.indexOf("italic") > -1;
        const isUnderLine = item.stype.indexOf("underline") > -1;
        const isOverline = item.stype.indexOf("lineThrough") > -1;
        const isBlue = item.stype.indexOf("blue") > -1;
        const isRed = item.stype.indexOf("red") > -1;
        const isGreen = item.stype.indexOf("green") > -1;
        const isBlueMarker = item.stype.indexOf("blue_hl") > -1;
        const isGreenMarker = item.stype.indexOf("green_hl") > -1;
        const isPinkMarker = item.stype.indexOf("pink_hl") > -1;
        const isPurpleMarker = item.stype.indexOf("purple_hl") > -1;
        const isYellowMarker = item.stype.indexOf("yellow_hl") > -1;
        const isOrangeMarker = item.stype.indexOf("orange_hl") > -1;
        let tag = "";

        switch (item.tag) {
          case "heading":
            tag = "h3";
            break;
          case "title":
            tag = "h1";
            break;
          case "body":
            tag = "p";
            break;
          case "ol":
            tag = "ol";
            break;
          case "ul":
            tag = "ul";
            break;

          default:
            tag = "p";
            break;
        }
        let styles = "";
        styles += isBold ? "font-weight: bold;" : "";
        styles += isItalic ? "font-style: italic;" : "";
        styles += isOverline ? "text-decoration: line-through;" : "";
        styles += isUnderLine ? "text-decoration: underline;" : "";
        styles += isBlue ? `color: ${availableStyles.blue.color};` : "";
        styles += isRed ? `color: ${availableStyles.red.color};` : "";
        styles += isGreen ? `color: ${availableStyles.green.color};` : "";
        styles += isBlueMarker
          ? `background-color: ${availableStyles.blue_hl.backgroundColor};`
          : "";
        styles += isGreenMarker
          ? `background-color: ${availableStyles.green_hl.backgroundColor};`
          : "";
        styles += isPinkMarker
          ? `background-color: ${availableStyles.pink_hl.backgroundColor};`
          : "";
        styles += isPurpleMarker
          ? `background-color: ${availableStyles.purple_hl.backgroundColor};`
          : "";
        styles += isYellowMarker
          ? `background-color: ${availableStyles.yellow_hl.backgroundColor};`
          : "";
        styles += isOrangeMarker
          ? `background-color: ${availableStyles.orange_hl.backgroundColor};`
          : "";

        if (item.NewLine == true || j == 0) {
          element = myDoc.createElement(tag);

          if (tag === "ol") {
            if (olStarted !== true) {
              olStarted = true;
              parent = myDoc.createElement(tag);
              myDoc.documentElement.appendChild(parent);
            }
            ulStarted = false;
            element = myDoc.createElement("li");
            parent.appendChild(element);
          } else if (tag === "ul") {
            if (ulStarted !== true) {
              ulStarted = true;
              parent = myDoc.createElement(tag);
              myDoc.documentElement.appendChild(parent);
            }
            olStarted = false;
            element = myDoc.createElement("li");
            parent.appendChild(element);
          } else {
            olStarted = false;
            ulStarted = false;

            element = myDoc.createElement(tag);
            myDoc.documentElement.appendChild(element);
          }
        }
        if (item.readOnly === true) {
        } else {
          const child = myDoc.createElement("span");
          if (item.NewLine === true && j != 0) {
            child.appendChild(myDoc.createTextNode(item.text.substring(1)));
          } else {
            child.appendChild(myDoc.createTextNode(item.text));
          }

          if (styles.length > 0) {
            child.setAttribute("style", styles);
          }

          element.appendChild(child);
        }
      }
    } else if (input.component === "image") {
      element = myDoc.createElement("img");
      element.setAttribute("src", input.url);
      element.setAttribute("width", input.size.width);
      element.setAttribute("height", input.size.height);
      if (input.imageId) {
        element.setAttribute("data-id", input.imageId);
      }
      myDoc.documentElement.appendChild(element);
    }
  }

  return new XMLSerializer().serializeToString(myDoc);
}
