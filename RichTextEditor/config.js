/**
 * File: config.js
 * Project: wpApps
 * File Created: 25 Nov 2020 12:11:43
 * Author: und3fined (me@und3fined.com)
 * -----
 * Last Modified: 25 Nov 2020 12:11:43
 * Modified By: und3fined (me@und3fined.com)
 * -----
 * Copyright (c) 2020 und3fined.com
 */
import React from "react";
import { Text } from "react-native";

export default (style, iconColor) => [
  {
    type: "tool",
    name: "bold",
    component: <Text>B</Text>,
  },
  {
    type: "tool",
    name: "italic",
    component: <Text>I</Text>,
  },
  {
    type: "tool",
    name: "underline",
    component: <Text>U</Text>,
  },
  {
    type: "tool",
    name: "strike",
    component: <Text>S</Text>,
  },
  {
    type: "seperator",
  },
  {
    type: "tool",
    name: "align-left",
    component: <Text>AL</Text>,
  },
  {
    type: "tool",
    name: "align-center",
    component: <Text>AC</Text>,
  },
  {
    type: "tool",
    name: "align-right",
    component: <Text>AR</Text>,
  },

  {
    type: "tool",
    name: "align-justify",
    component: <Text>AJ</Text>,
  },
  {
    type: "seperator",
  },
  {
    type: "tool",
    name: "link",
    component: <Text>URL</Text>,
  },
  {
    type: "tool",
    name: "image",
    component: <Text>IMG</Text>,
  },
  {
    type: "tool",
    name: "video",
    component: <Text>VID</Text>,
  },
];
