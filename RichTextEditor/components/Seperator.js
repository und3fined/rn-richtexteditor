/**
 * File: Seperator.js
 * Project: wpApps
 * File Created: 25 Nov 2020 10:14:31
 * Author: und3fined (me@und3fined.com)
 * -----
 * Last Modified: 25 Nov 2020 10:14:31
 * Modified By: und3fined (me@und3fined.com)
 * -----
 * Copyright (c) 2020 und3fined.com
 */
import React from 'react';
import { StyleSheet, View } from 'react-native';

export default ({ color, size }) => {
  const styleProps = {};
  if (color) styleProps.backgroundColor = color;
  if (size) styleProps.width = size;

  return <View style={[styles.separator, styleProps]} />;
};

const styles = StyleSheet.create({
  separator: {
    width: 1,
    marginVertical: 5,
    marginHorizontal: 3,
    backgroundColor: '#333333',
    borderRadius: 20,
  },
});
