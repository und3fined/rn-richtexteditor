/**
 * File: TextInput.js
 * Project: RichTextEditor
 * File Created: 25 Nov 2020 13:09:05
 * Author: und3fined (me@und3fined.com)
 * -----
 * Last Modified: 25 Nov 2020 13:09:05
 * Modified By: und3fined (me@und3fined.com)
 * -----
 * Copyright (c) 2020 und3fined.com
 */
import React, {
  useRef,
  useEffect,
  useState,
  useMemo,
  forwardRef,
  useImperativeHandle,
} from "react";
import { StyleSheet, TextInput } from "react-native";
import { isEqual, cloneDeep, concat, union } from "lodash";

import { useWillMount } from "../helpers/hook";
import {
  createContentItem,
  updateContentItem,
  appendContent,
  findItem,
  findIndex,
  getFulltext,
  getFulltextLength,
  getPrevText,
} from "../helpers/block";
import StyledText from "./StyledText";

let prevSelection = { start: 0, end: 0 };

export default forwardRef(
  (
    {
      items = [],
      style = {},
      returnKeyType = "next",
      hyperStyles,
      onHyperStyles = () => {},
      onChangeItem,
      ...props
    },
    parentRef
  ) => {
    const textInputRef = useRef();
    const [curSelection, setCurSelection] = useState({ start: 0, end: 0 });
    const [itemIndex, setItemIndex] = useState(-1);
    const [textContent, setTextContent] = useState("");
    const [tempContent, setTempContent] = useState([]);

    //============ HOOK ===========
    const content = useMemo(() => {
      if (isEqual(items, tempContent)) return tempContent;
      else return items;
    }, [items, tempContent]);

    useWillMount(() => {
      setTempContent(items);
      setItemIndex(0);
    });

    // useEffect(() => {
    //   if (itemIndex === -1 || !textContent) return;

    //   const { start, end } = curSelection;
    //   let newItems;
    //   let item = content[itemIndex];
    //   let fullText = getFulltext(content);

    //   if (isEqual(item.hyperStyles, hyperStyles)) {
    //     newItems = updateContentItem(items, itemIndex, {
    //       text: textContent,
    //       hyperStyles: item.hyperStyles,
    //       tag: item.tag,
    //       styles: item.styles,
    //     });
    //   } else {
    //     let oldText = fullText.slice(0, start);
    //     let newText = textContent.replace(oldText, "");
    //     let newItem = createContentItem(newText, "", hyperStyles);
    //     newItems = appendContent(items, newItem);
    //     if (itemIndex !== -1) setItemIndex(itemIndex + 1);
    //   }

    //   onChangeItem(newItems);
    // }, [textContent]);
    //============ @end HOOK ===========

    //============ HANDLER ============
    useImperativeHandle(parentRef, () => ({
      applyTools: (toolName) => {},
    }));

    const handleKeyDown = ({ nativeEvent }) => {
      const { start, end } = curSelection;
      if (nativeEvent.key === "Backspace" && start === 0 && end === 0) {
        if (props.onSelectPrev) props.onSelectPrev();
      }

      if (nativeEvent.key === "Backspace" && start === end) {
        console.info("Backspace...");
      }
    };

    const handleChangeText = (text) => {
      let fullText = getFulltext(content);
      if (fullText.length === 0) {
        let newItems = updateContentItem(content, 0, { text, hyperStyles });
        return onChangeItem(newItems);
      }

      const { start: prevStart, end: prevEnd } = prevSelection;
      const { start, end } = curSelection;
      if (start !== end) return;

      let newItems;
      const subtractText = text - fullText;
      const isAddText = subtractText > 0;
      const isRemText = subtractText <= 0;
      const contentLen = content.length - 1;
      const itemIndx = findIndex(content, end);

      console.info("itemIndx", itemIndx, prevEnd, content);

      // create new item content
      if (itemIndx === -1) {
        // find prev index from prev selection start
        let prevIndx = findIndex(content, start - 1);
        if (isEqual(prevIndx.hyperStyles, hyperStyles)) {
          newItems = updateContentItem(content, prevIndx, {
            text,
            hyperStyles,
          });
        } else {
          let newItem = createContentItem(text, "", hyperStyles);
          newItems = appendContent(content, newItem);
        }

        return onChangeItem(newItems);
      }

      // itemIndex is last of content
      if (itemIndx === contentLen) {
        let prevContent = "";
        for (let i = 0; i < itemIndx; i++) {
          prevContent += content[i].text;
        }
        let newText = text.replace(prevContent, "");
        newItems = updateContentItem(content, itemIndx, {
          text: newText,
          ...content[itemIndx],
        });

        return onChangeItem(newItems);
      }

      // let item = content[itemIndx];
      // let newItems;
      // let prevContent;
      // if (itemIndx === 0) {
      //   prevContent = "";
      // } else if (itemIndx === -1) {
      //   prevContent = fullText.slice(0, prevEnd);
      // } else {
      //   prevContent = getPrevText(content, itemIndx);
      // }

      // let newText = text.replace(prevContent, "");

      // if (item && isEqual(item.hyperStyles, hyperStyles)) {
      //   newItems = updateContentItem(content, itemIndx, {
      //     text: newText,
      //     hyperStyles: item.hyperStyles,
      //     tag: item.tag,
      //     styles: item.styles,
      //   });
      // } else {
      //   let newItem = createContentItem(newText, "", hyperStyles);
      //   newItems = appendContent(content, newItem);
      // }

      // onChangeItem(newItems);
    };

    const handleSelectionChange = ({ nativeEvent }) => {
      const { selection } = nativeEvent;
      prevSelection = cloneDeep(curSelection);

      const { start, end } = selection;

      if (start === end) {
        const idx = findIndex(content, end);
        const item = content[idx];
        const blockHyperStyles = union(
          content.hyperStyles || [],
          item ? item.hyperStyles : []
        );

        if (item && !isEqual(blockHyperStyles, hyperStyles)) {
          onHyperStyles(blockHyperStyles);
        }
      } else {
        const idx1 = findIndex(content, start < end ? start : end);
        const idx2 = findIndex(content, start > end ? start : end);

        if (idx1 === idx2) {
          const item = content[idx1];
          const blockHyperStyles = union(
            content.hyperStyles || [],
            item.hyperStyle
          );

          if (!isEqual(blockHyperStyles, hyperStyles)) {
            onHyperStyles(blockHyperStyles);
          }
        } else {
          let blockHyperStyles = [content.hyperStyles || []];
          for (let i = idx1; i <= idx2; i++) {
            blockHyperStyles = union(blockHyperStyles, content[i].hyperStyles);
          }

          if (!isEqual(blockHyperStyles, hyperStyles)) {
            onHyperStyles(blockHyperStyles);
          }
        }
      }

      setCurSelection(end < start ? { start: end, end: start } : selection);
    };

    const handleFocus = () => {};

    const handleBlur = () => {};

    const handleContentSizeChange = () => {};
    //============ @end HANDLER ============

    return (
      <TextInput
        ref={textInputRef}
        placeholder={props.placeholder}
        placeholderTextColor={props.placeholderTextColor}
        multiline={true}
        scrollEnabled={false}
        underlineColorAndroid="transparent"
        returnKeyType={returnKeyType}
        keyboardType="default"
        onKeyPress={handleKeyDown}
        onChangeText={handleChangeText}
        onSelectionChange={handleSelectionChange}
        onFocus={handleFocus}
        onBlur={handleBlur}
        onContentSizeChange={handleContentSizeChange}
        style={[styles.textInput, style]}
      >
        {content.map((item, i) => {
          return <StyledText key={`item-${i}`} item={item} />;
        })}
      </TextInput>
    );
  }
);

const styles = StyleSheet.create({
  textInput: {
    paddingVertical: 5,
    paddingHorizontal: 2,
    textAlignVertical: "top",
  },
});
