/**
 * File: StyledText.js
 * Project: RichTextEditor
 * File Created: 25 Nov 2020 16:11:50
 * Author: und3fined (me@und3fined.com)
 * -----
 * Last Modified: 25 Nov 2020 16:11:51
 * Modified By: und3fined (me@und3fined.com)
 * -----
 * Copyright (c) 2020 und3fined.com
 */
import React, { Component } from "react";
import { StyleSheet, Text } from "react-native";
import { isEqual, concat } from "lodash";

import defaultStyles from "../helpers/styles";

class StyledText extends Component {
  constructor(props) {
    super(props);
  }

  shouldComponentUpdate(nextProps) {
    return !isEqual(this.props.item, nextProps.item);
  }

  render() {
    const { styles = [], text, hyperStyles } = this.props.item;

    let hStyles = hyperStyles.map((hStyle) => defaultStyles[hStyle] || {});
    if (hyperStyles["underline"] && hyperStyles["strike"]) {
      hStyles.append(defaultStyles["underline strike"]);
    }

    let style = concat(styles, hStyles);
    return <Text style={StyleSheet.flatten(style)}>{text}</Text>;
  }
}

export default StyledText;
