/**
 * File: Icon.js
 * Project: wpApps
 * File Created: 25 Nov 2020 09:50:08
 * Author: und3fined (me@und3fined.com)
 * -----
 * Last Modified: 25 Nov 2020 09:50:08
 * Modified By: und3fined (me@und3fined.com)
 * -----
 * Copyright (c) 2020 und3fined.com
 */
import React from "react";
import { View, TouchableOpacity, TouchableWithoutFeedback } from "react-native";

export default ({
  name,
  toolSpacer,
  onSelect,
  active = false,
  defaultColor = "#222222",
  activeColor,
  iconStyles,
  activeBackground,
  component,
}) => {
  const iconViewProps = {};
  iconViewProps.style = {
    width: 36,
    height: 36,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 6,
    overflow: "hidden",
    ...iconStyles,
  };

  const iconProps = {};
  iconProps.style = {
    width: 24,
    height: 24,
    textAlign: "center",
    color: defaultColor,
  };

  if (active) {
    iconViewProps.style.backgroundColor = activeBackground;
    iconProps.fill = activeColor;
    iconProps.style.color = activeColor;
  }

  return (
    <TouchableOpacity
      accessible={false}
      style={{ marginHorizontal: toolSpacer ? toolSpacer / 2 : 3 }}
      activeOpacity={0.6}
      onPress={() => onSelect(name)}
    >
      <View {...iconViewProps}>
        {component ? (
          React.cloneElement(component, { ...iconProps })
        ) : (
          <Text {...iconProps}>{name}</Text>
        )}
      </View>
    </TouchableOpacity>
  );
};
