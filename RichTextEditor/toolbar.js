/**
 * File: toolbar.js
 * Project: wpApps
 * File Created: 25 Nov 2020 09:48:47
 * Author: und3fined (me@und3fined.com)
 * -----
 * Last Modified: 25 Nov 2020 09:48:48
 * Modified By: und3fined (me@und3fined.com)
 * -----
 * Copyright (c) 2020 und3fined.com
 */
import React from "react";
import {
  StyleSheet,
  View,
  ScrollView,
  Text,
  TouchableOpacity,
  Keyboard,
  useWindowDimensions,
} from "react-native";

import update from "immutability-helper";

import ButtonIcon from "./components/Icon";
import Seperator from "./components/Seperator";

export default ({
  toolbarStyle,
  seperatorStyle,
  config,
  onSelect = () => {},
  actives = [],
  ...buttonProps
}) => {
  const window = useWindowDimensions();
  const handleSelectTool = (tool) => {
    let toolIndex = actives.indexOf(tool);
    let newTools = [];
    if (toolIndex === -1) {
      newTools = update(actives, { $push: [tool] });
    } else {
      newTools = update(actives, { $splice: [[toolIndex, 1]] });
    }

    onSelect(newTools);
  };

  if (Array.isArray(config) === false) {
    return (
      <View style={[styles.container, toolbarStyle]}>
        <Text
          style={{
            flex: 1,
            color: "#e58e1d",
            height: 42,
            lineHeight: 42,
            textAlign: "center",
          }}
        >
          No config for Toolbar.
        </Text>
      </View>
    );
  }

  return (
    <View style={[styles.container, toolbarStyle, { width: window.width }]}>
      <ScrollView
        style={styles.scollbar}
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        keyboardShouldPersistTaps="handled"
      >
        {config.map((btn, i) => {
          if (btn.type === "seperator") {
            return <Seperator key={`toolbar-${i}`} {...seperatorStyle} />;
          } else {
            return (
              <ButtonIcon
                key={`toolbar-${i}`}
                active={actives.indexOf(btn.name) !== -1}
                onSelect={handleSelectTool}
                {...buttonProps}
                {...btn}
              />
            );
          }
        })}
      </ScrollView>
      <TouchableOpacity onPress={() => Keyboard.dismiss()}>
        <Text>Key</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    paddingVertical: 3,
    backgroundColor: "#eeeeee",
  },
  scollbar: {
    flex: 1,
    flexDirection: "row",
  },
});
