/**
 * File: editor.js
 * Project: wpApps
 * File Created: 25 Nov 2020 09:48:42
 * Author: und3fined (me@und3fined.com)
 * -----
 * Last Modified: 25 Nov 2020 09:48:42
 * Modified By: und3fined (me@und3fined.com)
 * -----
 * Copyright (c) 2020 und3fined.com
 */
import React, { useMemo, useRef, useEffect, useState, forwardRef } from "react";
import { View, ScrollView, Platform, useWindowDimensions } from "react-native";

import TextInput from "./components/TextInput";
import { updateBlock } from "./helpers/block";

const IS_IOS = Platform.OS === "ios";
let textInputs = [];

function resetVariable() {
  textInputs = [];
}

export default ({
  hyperStyles = [],
  onHyperStyles = () => {},
  content,
  onChangeContent,
  ...props
}) => {
  const scollViewRef = useRef();

  const handleScroll = () => {};
  const handleHyperStyles = (hyperStyles) => {
    console.info("handleHyperStyles,", hyperStyles);
    onHyperStyles(hyperStyles);
  };
  const handleChangeItem = (index, items) => {
    let newContent = updateBlock(content, index, items);
    onChangeContent(newContent);
  };

  return (
    <View style={{ flexGrow: 1, backgroundColor: "#333333" }}>
      <ScrollView
        style={{ flex: 1, backgroundColor: "#ff00ff" }}
        ref={scollViewRef}
        onScroll={handleScroll}
        scrollEventThrottle={1}
      >
        {content.map((item, index) => {
          let isLast = content.length - 1 === index;
          if (item.component === "text") {
            return (
              <RenderText
                key={`text-input-${index}`}
                ref={(input) => (textInputs[index] = input)}
                hyperStyles={hyperStyles}
                onHyperStyles={handleHyperStyles}
                onChangeItem={(items) => handleChangeItem(index, items)}
                items={item.content}
                other={props}
                isLast={isLast}
              />
            );
          }
        })}
      </ScrollView>
    </View>
  );
};

const RenderText = forwardRef(({ isLast, items, ...props }, parentRef) => {
  return (
    <View
      style={{
        marginBottom: 5,
        flexGrow: isLast === true ? 1 : 0,
        backgroundColor: "#ff0",
      }}
    >
      <TextInput
        ref={parentRef}
        onChangeItem={props.onChangeItem}
        hyperStyles={props.hyperStyles}
        onHyperStyles={props.onHyperStyles}
        placeholder={props.other.placeholder || ""}
        multiline={true}
        items={items}
        style={{ flexGrow: 1 }}
      />
    </View>
  );
});
