# Rich Text Editor

for React Native

### Features

- No webview
- Text style: **Bold**, _Italic_, _Underline_, **Line Though (Strike)**
- Align text: Left, Right, Center, Justify
- Listing: Dot, Number
- ...

### Getting code

Install dependencies with `yarn install` or `npm install`

Start preview in expo

```
expo start
```
